/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.telnet.command;
import l1j.server.Config;//導配置文件
import l1j.server.Base64;//導入密碼文件
import java.security.MessageDigest;
import java.util.StringTokenizer;

//import l1j.server.server.utils.SystemUtil;

import static l1j.server.telnet.command.TelnetCommandResult.*;

public class TelnetCommandExecutor {
	private static TelnetCommandExecutor _instance = new TelnetCommandExecutor();

	public static TelnetCommandExecutor getInstance() {
		return _instance;
	}

	public TelnetCommandResult execute(String cmd) {
		try {
			String TelNet_PassWord;
			TelNet_PassWord = Config.DB_PASSWORD;
	
			
			byte abyte1[];
			byte abyte2[];
			MessageDigest messagedigest = MessageDigest.getInstance("SHA");
			byte abyte0[] = TelNet_PassWord.getBytes("UTF-8");
			abyte1 = messagedigest.digest(abyte0);		
			TelNet_PassWord = Base64.encodeBytes(abyte1);
					
			
			StringTokenizer tok = new StringTokenizer(cmd, " ");
			
			String password = tok.nextToken();
			abyte2 = Base64.decode(password);
			boolean flag1 = false;
			
				flag1 = true;
				int i = 0;
				do {
					if (i >= abyte2.length) {
						break;
					}
					if (abyte1[i] != abyte2[i]) {
						flag1 = false;
						break;
					}
					i++;
				} while (true);

			
			
			if (flag1 == false){
				return new TelnetCommandResult(CMD_INTERNAL_ERROR, " TelNet PassWord ERROR");
				
			}
			String name = tok.nextToken();
			TelnetCommand command = TelnetCommandList.get(name);
			if (command == null) {
				return new TelnetCommandResult(CMD_NOT_FOUND, cmd
						+ " not found");
			}

			String args = "";
		
			args = cmd.substring(password.length() + 1);
		
			if (name.length() + 1 < cmd.length()) {
				args = args.substring(name.length() + 1);
			}
			System.out.println("Telnet:" + args);
			return command.execute(args);
		} catch (Exception e) {
			return new TelnetCommandResult(CMD_INTERNAL_ERROR, e
					.getLocalizedMessage());
		}
	}
}
