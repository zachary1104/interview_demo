/**
 * 		This object is created for auto heal system. Once the thread start the 
 * 		this system will start to monitor specific player's HP and the Default setting 
 * 		is if the player's HP is less than 100%, the system start to take potion until the
 * 		player's HP hit the setting. The system also reserve the function which allow player
 * 		to set up how many %, the system start to take potion.
 * 		copyright by zachary 01/02/2010 
 */
package l1j.server.server.features;

import static l1j.server.server.model.skill.L1SkillId.ABSOLUTE_BARRIER;
import static l1j.server.server.model.skill.L1SkillId.POLLUTE_WATER;
import java.util.Random;

import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;

public class AutoHeallingSystem implements Runnable {

	private L1PcInstance _pc;
	
	//預設低於100%就是喝水
	private double _percentage = 1;
	
	private static Random _random = new Random();
	
	public AutoHeallingSystem(L1PcInstance pc) {
		// TODO Auto-generated constructor stub
		_pc = pc;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		autoUseHeallingPotion();
	}
	/**
	 * 	玩家設定HP低於多少%時,開始喝水
	 * @param percentage
	 */
	public void setPercentage(double percentage){
		_percentage = percentage /100;
	}
	/**
	 * 目的 : 	auto heal player self by using recursive function
	 * 			The function has one major if-else statement with specific condition.
	 * 			In case, if the player doesn't set up the percentage when the system will starts
	 * 			to use heal potion, the system will start with 100% automatically.
	 * 			  		
	 * @param activate
	 * @author Zachary
	 * @throws InterruptedException 
	 */
	private boolean autoUseHeallingPotion(){		
		while(_pc.isActivateAutoHeal()){
			if(_pc.isDead()){
				_pc.setDeactivateAutoHeal(false);
				_pc.setCurrentHp(0);
				_pc.removeSkillEffect(L1SkillId.STATUS_AUTO_HEALING_SYSTEM);
				break;
			}
			try{
				if(!_pc.hasSkillEffect(33)&&!_pc.hasSkillEffect(50)&&!_pc.hasSkillEffect(80)){
					if(_pc.getCurrentHp() < (_pc.getMaxHp() * _percentage)){
						if(!_pc.getInventory().checkItem(40043)){
							_pc.sendPackets(new S_SystemMessage("沒有具有 古代終極體力恢復劑"));
							continue;
						}
						if(!_pc.hasSkillEffect(ABSOLUTE_BARRIER)){   //假如有施放絕對屏障不喝水
							UseHeallingPotion(600, 189);
							_pc.getInventory().removeItem(_pc.getInventory().findItemId(40043), 1);
						}
						Thread.sleep(500);
					}
				}
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		_pc.sendPackets(new S_SystemMessage("自動喝水系統 關閉"));
		return false;
	}
	private void UseHeallingPotion(int healHp, int gfxid) {
		if (_pc.hasSkillEffect(71) == true) { 
			_pc.sendPackets(new S_ServerMessage(698)); 
			return;
		}
		_pc.sendPackets(new S_SkillSound(_pc.getId(), gfxid));
		_pc.broadcastPacket(new S_SkillSound(_pc.getId(), gfxid));
		_pc.sendPackets(new S_ServerMessage(77)); 
		healHp *= (_random.nextGaussian() / 5.0D) + 1.0D;
		if (_pc.hasSkillEffect(POLLUTE_WATER)) { //假如重了妖經的污濁之水的話喝水效果減半
			healHp /= 2;
		}
		_pc.setCurrentHp(_pc.getCurrentHp() + healHp);
	}
	
}
