package l1j.server.server.features;

public class L1Castleguard
{
	private int _CASTLE;
	private int _TYPE;
	private int _MOB;
	private int _X;
	private int _Y;
	private int _MAP;
	private int _HEAD;
	public L1Castleguard(){}
	public L1Castleguard(int castle, int type, int mob
		, int x, int y, int map, int head)
	{
		_CASTLE = castle;		_TYPE = type;
		_MOB = mob;	_X = x;			_Y = y;
		_MAP = map;	_HEAD = head;
	}
	public void set_CASTLE(int i)
	{
		_CASTLE = i; 
	}
	public int get_CASTLE()
	{
		return _CASTLE;
	}
	public void set_TYPE(int i)
	{
		_TYPE = i; 
	}
	public int get_TYPE()
	{
		return _TYPE;
	}
	public void set_MOB(int i)
	{
		_MOB = i; 
	}
	public int get_MOB()
	{
		return _MOB;
	}
	public void set_X(int i)
	{
		_X = i; 
	}
	public int get_X()
	{
		return _X;
	}
	public void set_Y(int i)
	{
		_Y = i; 
	}
	public int get_Y()
	{
		return _Y;
	}
	public void set_MAP(int i)
	{
		_MAP = i; 
	}
	public int get_MAP()
	{
		return _MAP;
	}
	public void set_HEAD(int i)
	{
		_HEAD = i; 
	}
	public int get_HEAD()
	{
		return _HEAD;
	}
}