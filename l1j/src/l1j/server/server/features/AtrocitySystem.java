/*
 * */
package l1j.server.server.features;

import java.util.Random;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_TrueTarget;
import static l1j.server.server.model.skill.L1SkillId.*;

public class AtrocitySystem implements Runnable {

	private L1PcInstance _pc;

	public AtrocitySystem(L1PcInstance pc) {
		if (pc instanceof L1PcInstance) {
			_pc = (L1PcInstance) pc;
		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		startFrantic();
		System.out.println("Thread end");
	}

	public void startFrantic() {
		while (!_pc.isDead()) {
			if (!_pc.hasSkillEffect(SPECIAL_ATTACTION_STAGE_1)
					|| !_pc.hasSkillEffect(SPECIAL_ATTACTION_STAGE_2)) {
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (_pc.hasSkillEffect(SPECIAL_ATTACTION_STAGE_1)) {
				Random random = new Random();
				if (random.nextInt(100) + 1 <= 15) { // 進入第二階段的機率 15%
					_pc.setSkillEffect(SPECIAL_ATTACTION_STAGE_2, 10000); // 進入第二階段後維持的時間10秒
					_pc.removeSkillEffect(SPECIAL_ATTACTION_STAGE_1);
					_pc.sendPackets(new S_SkillSound(_pc.getId(), 3909));
					_pc.broadcastPacket(new S_SkillSound(_pc.getId(), 3909));
					_pc.sendPackets(new S_SystemMessage("\\fR進入第二階段憤怒"));
					_pc.sendPackets(new S_TrueTarget(_pc.getId(), _pc.getId(),
							"進入第二階段憤怒"));

				}
			} else if (_pc.hasSkillEffect(SPECIAL_ATTACTION_STAGE_2)) {
				Random random = new Random();
				if (random.nextInt(100) + 1 <= 3) {// 進入第三階段的機率3%
					_pc.setSkillEffect(SPECIAL_ATTACTION_STAGE_3, 30000);// 進入第三階段後維持的時間30秒
					_pc.removeSkillEffect(SPECIAL_ATTACTION_STAGE_2);

					_pc.broadcastPacket(new S_SkillSound(_pc.getId(), 6576));
					_pc.sendPackets(new S_SystemMessage("\\fR進入憤怒瘋恐殘暴狀態狀態"));

				}
			}
		}
	}
}
