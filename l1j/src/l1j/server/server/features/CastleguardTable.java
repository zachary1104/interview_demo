package l1j.server.server.features;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.utils.SQLUtil;
public class CastleguardTable
{
	private static ArrayList<L1Castleguard> castleguardlist = new ArrayList<L1Castleguard>();
	private static CastleguardTable _instance;
	public static CastleguardTable getInstance() 
	{
		if (_instance == null) 
		{
			_instance = new CastleguardTable();
		}
		return _instance;
	}
	public CastleguardTable()
	{
		load();
	}
	public ArrayList<L1Castleguard> get_castleguard(int i, int t)
	{
		ArrayList<L1Castleguard> list = new ArrayList<L1Castleguard>();
		for(L1Castleguard castleguard : castleguardlist)
		{
			if(castleguard.get_CASTLE() == i && castleguard.get_TYPE() == t)
			{
				list.add(castleguard);
			}
		}
		return list;
	}
	public void load()
	{
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try 
		{
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM spawnlist_castleguard ORDER BY id");
			rs = pstm.executeQuery();
			while (rs.next()) 
			{
				L1Castleguard castleguard = new L1Castleguard();
				castleguard.set_CASTLE(rs.getInt("castleid"));
				castleguard.set_TYPE(rs.getInt("type"));
				castleguard.set_MOB(rs.getInt("mobid"));
				castleguard.set_X(rs.getInt("x"));
				castleguard.set_Y(rs.getInt("y"));
				castleguard.set_MAP(rs.getInt("mapid"));
				castleguard.set_HEAD(rs.getInt("Head"));
				castleguardlist.add(castleguard);
			}
		} catch (SQLException e) 
		{
		} finally 
		{
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
}