package l1j.server.server.features;

public class L1IpCount
{
	private String _IP;
	private long _TIMES;
	public L1IpCount(){}
	public L1IpCount(String ip, long time)
	{
		_IP = ip;
		_TIMES = time;
	}
	public void set_IP(String i)
	{
		_IP = i; 
	}
	public String get_IP()
	{
		return _IP;
	}
	public void set_TIMES(long i)
	{
		_TIMES = i; 
	}
	public long get_TIMES()
	{
		return _TIMES;
	}
}