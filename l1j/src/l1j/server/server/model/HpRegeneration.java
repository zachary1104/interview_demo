/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;

import l1j.server.server.model.Instance.L1EffectInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.types.Point;
import static l1j.server.server.model.skill.L1SkillId.*;

public class HpRegeneration extends TimerTask {

	private static Logger _log = Logger.getLogger(HpRegeneration.class
			.getName());

	private final L1PcInstance _pc;

	private int _regenMax = 0;

	private int _regenPoint = 0;

	private int _curPoint = 4;

	private static Random _random = new Random();

	public HpRegeneration(L1PcInstance pc) {
		_pc = pc;

		updateLevel();
	}

	public void setState(int state) {
		if (_curPoint < state) {
			return;
		}

		_curPoint = state;
	}

	@Override
	public void run() {
		try {
			if (_pc.isDead()) {
				return;
			}

			_regenPoint += _curPoint;
			_curPoint = 4;

			synchronized (this) {
				if (_regenMax <= _regenPoint) {
					_regenPoint = 0;
					regenHp();
				}
			}
		} catch (Throwable e) {
			_log.log(Level.WARNING, e.getLocalizedMessage(), e);
		}
	}

	public void updateLevel() {
		final int lvlTable[] = new int[] { 30, 25, 20, 16, 14, 12, 11, 10, 9,
				3, 2 };

		int regenLvl = Math.min(10, _pc.getLevel());
		if (30 <= _pc.getLevel() && _pc.isKnight()) {
			regenLvl = 11;
		}

		synchronized (this) {
			_regenMax = lvlTable[regenLvl - 1] * 4;
		}
	}

	public void regenHp() {
		if (_pc.isDead()) {
			return;
		}

		int maxBonus = 1;

		// CONボーナス
		if (11 < _pc.getLevel() && 14 <= _pc.getCon()) {
			maxBonus = _pc.getCon() - 12;
			if (25 < _pc.getCon()) {
				maxBonus = 14;
			}
		}

		int equipHpr = _pc.getInventory().hpRegenPerTick();
		equipHpr += _pc.getHpr();
		int bonus = _random.nextInt(maxBonus) + 1;

		if (_pc.hasSkillEffect(NATURES_TOUCH)) {
			bonus += 15;
		}
		if (L1HouseLocation.isInHouse(_pc.getX(), _pc.getY(), _pc.getMapId())) {
			bonus += 5;
		}
		if (_pc.getMapId() == 16384 || _pc.getMapId() == 16896
				|| _pc.getMapId() == 17408 || _pc.getMapId() == 17920
				|| _pc.getMapId() == 18432 || _pc.getMapId() == 18944
				|| _pc.getMapId() == 19968 || _pc.getMapId() == 19456
				|| _pc.getMapId() == 20480 || _pc.getMapId() == 20992
				|| _pc.getMapId() == 21504 || _pc.getMapId() == 22016
				|| _pc.getMapId() == 22528 || _pc.getMapId() == 23040
				|| _pc.getMapId() == 23552 || _pc.getMapId() == 24064
				|| _pc.getMapId() == 24576 || _pc.getMapId() == 25088
				|| _pc.getMapId() == 15 || _pc.getMapId() == 29
				|| _pc.getMapId() == 52 || _pc.getMapId() == 64
				|| _pc.getMapId() == 300 || _pc.getMapId() == 100
				|| _pc.getMapId() == 5001 || _pc.getMapId() == 5002
				|| _pc.getMapId() == 5003 || _pc.getMapId() == 5004
				|| _pc.getMapId() == 5005 || _pc.getMapId() == 5006
				|| _pc.getMapId() == 5007 || _pc.getMapId() == 5008
				|| _pc.getMapId() == 5009 || _pc.getMapId() == 5010
				|| _pc.getMapId() == 5011 || _pc.getMapId() == 5012
				|| _pc.getMapId() == 5013 || _pc.getMapId() == 5014
				|| _pc.getMapId() == 5015 || _pc.getMapId() == 5016
				|| _pc.getMapId() == 5017 || _pc.getMapId() == 5018
				|| _pc.getMapId() == 5019 || _pc.getMapId() == 5020
				|| _pc.getMapId() == 5021 || _pc.getMapId() == 5022
				|| _pc.getMapId() == 5023 || _pc.getMapId() == 5024
				|| _pc.getMapId() == 5025 || _pc.getMapId() == 5026
				|| _pc.getMapId() == 5027 || _pc.getMapId() == 5028
				|| _pc.getMapId() == 5029 || _pc.getMapId() == 5030
				|| _pc.getMapId() == 5031 || _pc.getMapId() == 5032
				|| _pc.getMapId() == 5033 || _pc.getMapId() == 5034
				|| _pc.getMapId() == 5035 || _pc.getMapId() == 5036
				|| _pc.getMapId() == 5037 || _pc.getMapId() == 5038
				|| _pc.getMapId() == 5039 || _pc.getMapId() == 5040
				|| _pc.getMapId() == 5041 || _pc.getMapId() == 5042
				|| _pc.getMapId() == 5043 || _pc.getMapId() == 5044
				|| _pc.getMapId() == 5045 || _pc.getMapId() == 5046
				|| _pc.getMapId() == 5047 || _pc.getMapId() == 5048
				|| _pc.getMapId() == 5049 || _pc.getMapId() == 5050
				|| _pc.getMapId() == 5051 || _pc.getMapId() == 5052
				|| _pc.getMapId() == 5053 || _pc.getMapId() == 5054
				|| _pc.getMapId() == 5055 || _pc.getMapId() == 5056
				|| _pc.getMapId() == 5057 || _pc.getMapId() == 5058
				|| _pc.getMapId() == 5059 || _pc.getMapId() == 5060
				|| _pc.getMapId() == 5061 || _pc.getMapId() == 5062
				|| _pc.getMapId() == 5063 || _pc.getMapId() == 5064
				|| _pc.getMapId() == 5065 || _pc.getMapId() == 5066
				|| _pc.getMapId() == 5067 || _pc.getMapId() == 5068
				|| _pc.getMapId() == 5069 || _pc.getMapId() == 5070
				|| _pc.getMapId() == 5071 || _pc.getMapId() == 5072
				|| _pc.getMapId() == 5073 || _pc.getMapId() == 5074
				|| _pc.getMapId() == 5075 || _pc.getMapId() == 5076
				|| _pc.getMapId() == 5077 || _pc.getMapId() == 5078
				|| _pc.getMapId() == 5079 || _pc.getMapId() == 5080
				|| _pc.getMapId() == 5081 || _pc.getMapId() == 5082
				|| _pc.getMapId() == 5083 || _pc.getMapId() == 5084
				|| _pc.getMapId() == 5085 || _pc.getMapId() == 5086
				|| _pc.getMapId() == 5087 || _pc.getMapId() == 5088
				|| _pc.getMapId() == 5089 || _pc.getMapId() == 5090
				|| _pc.getMapId() == 5091 || _pc.getMapId() == 5092
				|| _pc.getMapId() == 5093 || _pc.getMapId() == 5094
				|| _pc.getMapId() == 5095 || _pc.getMapId() == 5096
				|| _pc.getMapId() == 5097 || _pc.getMapId() == 5098
				|| _pc.getMapId() == 5099 || _pc.getMapId() == 5100
				|| _pc.getMapId() == 5101 || _pc.getMapId() == 5102
				|| _pc.getMapId() == 5103 || _pc.getMapId() == 5104
				|| _pc.getMapId() == 5105 || _pc.getMapId() == 5106
				|| _pc.getMapId() == 5107 || _pc.getMapId() == 5108
				|| _pc.getMapId() == 5109 || _pc.getMapId() == 5110
				|| _pc.getMapId() == 5111 || _pc.getMapId() == 5112
				|| _pc.getMapId() == 5113 || _pc.getMapId() == 5114
				|| _pc.getMapId() == 5115 || _pc.getMapId() == 5116
				|| _pc.getMapId() == 5117 || _pc.getMapId() == 5118
				|| _pc.getMapId() == 5119 || _pc.getMapId() == 5120
				|| _pc.getMapId() == 5121 || _pc.getMapId() == 5122
				|| _pc.getMapId() == 5123 || _pc.getMapId() == 350
				|| _pc.getMapId() == 1000
					
		) { 
			// 宿屋
//開始------------------------	#0033				
			bonus += 100;
//結束------------------------	#0033				
		}
		if ((_pc.getLocation().isInScreen(new Point(33055,32336))
				&& _pc.getMapId() == 4 && _pc.isElf())) {
			bonus += 5;
		}
 		if (_pc.hasSkillEffect(COOKING_1_5_N)
				|| _pc.hasSkillEffect(COOKING_1_5_S)) {
			bonus += 3;
		}
 		if (_pc.hasSkillEffect(COOKING_2_4_N)
				|| _pc.hasSkillEffect(COOKING_2_4_S)
				|| _pc.hasSkillEffect(COOKING_3_6_N)
				|| _pc.hasSkillEffect(COOKING_3_6_S)) {
			bonus += 2;
		}
 		if (_pc.getOriginalHpr() > 0) { // オリジナルCON HPR補正
 			bonus += _pc.getOriginalHpr();
 		}

		boolean inLifeStream = false;
		if (isPlayerInLifeStream(_pc)) {
			inLifeStream = true;
			// 古代の空間、魔族の神殿ではHPR+3はなくなる？
			bonus += 3;
		}

		// 空腹と重量のチェック
		if (_pc.get_food() < 3 || isOverWeight(_pc)
				|| _pc.hasSkillEffect(BERSERKERS)) {
			bonus = 0;
			// 装備によるＨＰＲ増加は満腹度、重量によってなくなるが、 減少である場合は満腹度、重量に関係なく効果が残る
			if (equipHpr > 0) {
				equipHpr = 0;
			}
		}

		int newHp = _pc.getCurrentHp();
		newHp += bonus + equipHpr;

		if (newHp < 1) {
			newHp = 1; // ＨＰＲ減少装備によって死亡はしない
		}
		// 水中での減少処理
		// ライフストリームで減少をなくせるか不明
		if (isUnderwater(_pc)) {
			newHp -= 20;
			if (newHp < 1) {
				if (_pc.isGm()) {
					newHp = 1;
				} else {
					_pc.death(null); // 窒息によってＨＰが０になった場合は死亡する。
				}
			}
		}
		// Lv50クエストの古代の空間1F2Fでの減少処理
		if (isLv50Quest(_pc) && !inLifeStream) {
			newHp -= 10;
			if (newHp < 1) {
				if (_pc.isGm()) {
					newHp = 1;
				} else {
					_pc.death(null); // ＨＰが０になった場合は死亡する。
				}
			}
		}
		// 魔族の神殿での減少処理
		if (_pc.getMapId() == 410 && !inLifeStream) {
			newHp -= 10;
			if (newHp < 1) {
				if (_pc.isGm()) {
					newHp = 1;
				} else {
					_pc.death(null); // ＨＰが０になった場合は死亡する。
				}
			}
		}

		if (!_pc.isDead()) {
			_pc.setCurrentHp(Math.min(newHp, _pc.getMaxHp()));
		}
	}

	private boolean isUnderwater(L1PcInstance pc) {
		// ウォーターブーツ装備時か、 エヴァの祝福状態、修理された装備セットであれば水中では無いとみなす。
		if (pc.getInventory().checkEquipped(20207)) {
			return false;
		}
		if (pc.hasSkillEffect(STATUS_UNDERWATER_BREATH)) {
			return false;
		}
		if (pc.getInventory().checkEquipped(21048)
				&& pc.getInventory().checkEquipped(21049)
				&& pc.getInventory().checkEquipped(21050)) {
			return false;
		}

		return pc.getMap().isUnderwater();
	}

	private boolean isOverWeight(L1PcInstance pc) {
		// エキゾチックバイタライズ状態、アディショナルファイアー状態か
		// ゴールデンウィング装備時であれば、重量オーバーでは無いとみなす。
		if (pc.hasSkillEffect(EXOTIC_VITALIZE)
				|| pc.hasSkillEffect(ADDITIONAL_FIRE)) {
			return false;
		}
		if (pc.getInventory().checkEquipped(20049)) {
			return false;
		}

		return (120 <= pc.getInventory().getWeight240()) ? true : false;
	}

	private boolean isLv50Quest(L1PcInstance pc) {
		int mapId = pc.getMapId();
		return (mapId == 2000 || mapId == 2001) ? true : false;
	}

	/**
	 * 指定したPCがライフストリームの範囲内にいるかチェックする
	 * 
	 * @param pc
	 *            PC
	 * @return true PCがライフストリームの範囲内にいる場合
	 */
	private static boolean isPlayerInLifeStream(L1PcInstance pc) {
		for (L1Object object : pc.getKnownObjects()) {
			if (object instanceof L1EffectInstance == false) {
				continue;
			}
			L1EffectInstance effect = (L1EffectInstance) object;
			if (effect.getNpcId() == 81169 && effect.getLocation()
					.getTileLineDistance(pc.getLocation()) < 4) {
				return true;
			}
		}
		return false;
	}
}
