package l1j.server.server.model;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.types.Point;
import static l1j.server.server.model.skill.L1SkillId.*;

public class MpRegeneration extends TimerTask {
	private static Logger _log = Logger.getLogger(MpRegeneration.class
			.getName());

	private final L1PcInstance _pc;

	private int _regenPoint = 0;

	private int _curPoint = 4;

	public MpRegeneration(L1PcInstance pc) {
		_pc = pc;
	}

	public void setState(int state) {
		if (_curPoint < state) {
			return;
		}

		_curPoint = state;
	}

	@Override
	public void run() {
		try {
			if (_pc.isDead()) {
				return;
			}

			_regenPoint += _curPoint;
			_curPoint = 4;

			if (64 <= _regenPoint) {
				_regenPoint = 0;
				regenMp();
			}
		} catch (Throwable e) {
			_log.log(Level.WARNING, e.getLocalizedMessage(), e);
		}
	}

	public void regenMp() {
		int baseMpr = 1;
		int wis = _pc.getWis();
		if (wis == 15 || wis == 16) {
			baseMpr = 2;
		} else if (wis >= 17) {
			baseMpr = 3;
		}

		if (_pc.hasSkillEffect(STATUS_BLUE_POTION)) { // ブルーポーション使用中
			if (wis < 11) { // WIS11未満でもMPR+1
				wis = 11;
			}
			baseMpr += wis - 10;
		}
		if (_pc.hasSkillEffect(MEDITATION)) { // メディテーション中
			baseMpr += 5;
		}
		if (_pc.hasSkillEffect(CONCENTRATION)) { // コンセントレーション中
			baseMpr += 2;
		}
		if (L1HouseLocation.isInHouse(_pc.getX(), _pc.getY(), _pc.getMapId())) {
			baseMpr += 3;
		}
		if (_pc.getMapId() == 16384 || _pc.getMapId() == 16896
				|| _pc.getMapId() == 17408 || _pc.getMapId() == 17920
				|| _pc.getMapId() == 18432 || _pc.getMapId() == 18944
				|| _pc.getMapId() == 19968 || _pc.getMapId() == 19456
				|| _pc.getMapId() == 20480 || _pc.getMapId() == 20992
				|| _pc.getMapId() == 21504 || _pc.getMapId() == 22016
				|| _pc.getMapId() == 22528 || _pc.getMapId() == 23040
				|| _pc.getMapId() == 23552 || _pc.getMapId() == 24064
				|| _pc.getMapId() == 24576 || _pc.getMapId() == 25088
				|| _pc.getMapId() == 15 || _pc.getMapId() == 29
				|| _pc.getMapId() == 52 || _pc.getMapId() == 64
				|| _pc.getMapId() == 300 || _pc.getMapId() == 100
				|| _pc.getMapId() == 5001 || _pc.getMapId() == 5002
				|| _pc.getMapId() == 5003 || _pc.getMapId() == 5004
				|| _pc.getMapId() == 5005 || _pc.getMapId() == 5006
				|| _pc.getMapId() == 5007 || _pc.getMapId() == 5008
				|| _pc.getMapId() == 5009 || _pc.getMapId() == 5010
				|| _pc.getMapId() == 5011 || _pc.getMapId() == 5012
				|| _pc.getMapId() == 5013 || _pc.getMapId() == 5014
				|| _pc.getMapId() == 5015 || _pc.getMapId() == 5016
				|| _pc.getMapId() == 5017 || _pc.getMapId() == 5018
				|| _pc.getMapId() == 5019 || _pc.getMapId() == 5020
				|| _pc.getMapId() == 5021 || _pc.getMapId() == 5022
				|| _pc.getMapId() == 5023 || _pc.getMapId() == 5024
				|| _pc.getMapId() == 5025 || _pc.getMapId() == 5026
				|| _pc.getMapId() == 5027 || _pc.getMapId() == 5028
				|| _pc.getMapId() == 5029 || _pc.getMapId() == 5030
				|| _pc.getMapId() == 5031 || _pc.getMapId() == 5032
				|| _pc.getMapId() == 5033 || _pc.getMapId() == 5034
				|| _pc.getMapId() == 5035 || _pc.getMapId() == 5036
				|| _pc.getMapId() == 5037 || _pc.getMapId() == 5038
				|| _pc.getMapId() == 5039 || _pc.getMapId() == 5040
				|| _pc.getMapId() == 5041 || _pc.getMapId() == 5042
				|| _pc.getMapId() == 5043 || _pc.getMapId() == 5044
				|| _pc.getMapId() == 5045 || _pc.getMapId() == 5046
				|| _pc.getMapId() == 5047 || _pc.getMapId() == 5048
				|| _pc.getMapId() == 5049 || _pc.getMapId() == 5050
				|| _pc.getMapId() == 5051 || _pc.getMapId() == 5052
				|| _pc.getMapId() == 5053 || _pc.getMapId() == 5054
				|| _pc.getMapId() == 5055 || _pc.getMapId() == 5056
				|| _pc.getMapId() == 5057 || _pc.getMapId() == 5058
				|| _pc.getMapId() == 5059 || _pc.getMapId() == 5060
				|| _pc.getMapId() == 5061 || _pc.getMapId() == 5062
				|| _pc.getMapId() == 5063 || _pc.getMapId() == 5064
				|| _pc.getMapId() == 5065 || _pc.getMapId() == 5066
				|| _pc.getMapId() == 5067 || _pc.getMapId() == 5068
				|| _pc.getMapId() == 5069 || _pc.getMapId() == 5070
				|| _pc.getMapId() == 5071 || _pc.getMapId() == 5072
				|| _pc.getMapId() == 5073 || _pc.getMapId() == 5074
				|| _pc.getMapId() == 5075 || _pc.getMapId() == 5076
				|| _pc.getMapId() == 5077 || _pc.getMapId() == 5078
				|| _pc.getMapId() == 5079 || _pc.getMapId() == 5080
				|| _pc.getMapId() == 5081 || _pc.getMapId() == 5082
				|| _pc.getMapId() == 5083 || _pc.getMapId() == 5084
				|| _pc.getMapId() == 5085 || _pc.getMapId() == 5086
				|| _pc.getMapId() == 5087 || _pc.getMapId() == 5088
				|| _pc.getMapId() == 5089 || _pc.getMapId() == 5090
				|| _pc.getMapId() == 5091 || _pc.getMapId() == 5092
				|| _pc.getMapId() == 5093 || _pc.getMapId() == 5094
				|| _pc.getMapId() == 5095 || _pc.getMapId() == 5096
				|| _pc.getMapId() == 5097 || _pc.getMapId() == 5098
				|| _pc.getMapId() == 5099 || _pc.getMapId() == 5100
				|| _pc.getMapId() == 5101 || _pc.getMapId() == 5102
				|| _pc.getMapId() == 5103 || _pc.getMapId() == 5104
				|| _pc.getMapId() == 5105 || _pc.getMapId() == 5106
				|| _pc.getMapId() == 5107 || _pc.getMapId() == 5108
				|| _pc.getMapId() == 5109 || _pc.getMapId() == 5110
				|| _pc.getMapId() == 5111 || _pc.getMapId() == 5112
				|| _pc.getMapId() == 5113 || _pc.getMapId() == 5114
				|| _pc.getMapId() == 5115 || _pc.getMapId() == 5116
				|| _pc.getMapId() == 5117 || _pc.getMapId() == 5118
				|| _pc.getMapId() == 5119 || _pc.getMapId() == 5120
				|| _pc.getMapId() == 5121 || _pc.getMapId() == 5122
				|| _pc.getMapId() == 5123 || _pc.getMapId() == 350
				|| _pc.getMapId() == 1000
		) { 
			// 宿屋
			
//開始----------------------#0035				
			baseMpr += 100;
//結束----------------------#0035				
		}
		if ((_pc.getLocation().isInScreen(new Point(33055,32336))
				&& _pc.getMapId() == 4 && _pc.isElf())) {
			baseMpr += 3;
		}
		if (_pc.hasSkillEffect(COOKING_1_2_N)
				|| _pc.hasSkillEffect(COOKING_1_2_S)) {
			baseMpr += 3;
		}
 		if (_pc.hasSkillEffect(COOKING_2_4_N)
				|| _pc.hasSkillEffect(COOKING_2_4_S)
				|| _pc.hasSkillEffect(COOKING_3_5_N)
				|| _pc.hasSkillEffect(COOKING_3_5_S)) {
			baseMpr += 2;
		}
 		if (_pc.getOriginalMpr() > 0) { // オリジナルWIS MPR補正
 			baseMpr += _pc.getOriginalMpr();
 		}

		int itemMpr = _pc.getInventory().mpRegenPerTick();
		itemMpr += _pc.getMpr();

		if (_pc.get_food() < 3 || isOverWeight(_pc)) {
			baseMpr = 0;
			if (itemMpr > 0) {
				itemMpr = 0;
			}
		}
		int mpr = baseMpr + itemMpr;
		int newMp = _pc.getCurrentMp() + mpr;
		if (newMp < 0) {
			newMp = 0;
		}
		_pc.setCurrentMp(newMp);
	}

	private boolean isOverWeight(L1PcInstance pc) {
		// エキゾチックバイタライズ状態、アディショナルファイアー状態であれば、
		// 重量オーバーでは無いとみなす。
		if (pc.hasSkillEffect(EXOTIC_VITALIZE)
				|| pc.hasSkillEffect(ADDITIONAL_FIRE)) {
			return false;
		}

		return (120 <= pc.getInventory().getWeight240()) ? true : false;
	}
}
