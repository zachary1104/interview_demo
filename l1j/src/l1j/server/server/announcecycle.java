
package l1j.server.server;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import l1j.server.Config;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SystemMessage;
import java.util.Collection;
import javolution.util.FastList; 

public class announcecycle {
        private static Logger _log = Logger.getLogger(announcecycle.class.getName()); 

    private static announcecycle _instance;

    //private List<String> _announcecycle = new FastList<String>();//加入循環公告元件型字串陣列組 
    private List<String> _announcecycle = new FastList<String>();//加入循環公告元件型字串陣列組
        private int _announcecyclesize = 0;

    private announcecycle() {
        loadannouncecycle();
    }

    public static announcecycle getInstance() {
        if (_instance == null) {
            _instance = new announcecycle();
        }

        return _instance;
    }
      /** The task launching the function doAnnouncCycle() */ 
    class AnnouncTask implements Runnable 
    { 
    public void run() 
    { 
    try 
    { 
    showannouncecycle(_announcecycle.get(_announcecyclesize));//輪迴式公告發布 
    _announcecyclesize++; 
    if(_announcecyclesize>=_announcecycle.size())//公告折返回第1則 
    _announcecyclesize = 0; 
    } 
    catch (Exception e) 
    { 
    _log.log(Level.WARNING, "", e); 
    } 
    } 
    } 

    public void doannouncecycle() 
    { 
    AnnouncTask rs = new AnnouncTask();//建立執行緒 
    
    

    GeneralThreadPool.getInstance().scheduleAtFixedRate(rs, Config.NOTICE_STARTIME, Config.NOTICE_LOOPTIME); 
    //    使用低階排程運作 時間間格可以在這裡設定改 1200*1000 的1200部分為單位秒 
    } 

    public void loadannouncecycle()//讀取循環公告 
    { 
    _announcecycle.clear(); 
    File file = new File("data/announcecycle.txt"); 
    if (file.exists()) 
    { 
    readFromDiskmulti(file); 
    doannouncecycle();//若有載入檔案即開始運作循環公告執行緒 
    } 
    else 
    { 
    _log.config("data/announcecycle.txt 檔案不存在"); 
    } 
    } 
    private void readFromDiskmulti(File file) 
    { //循環公告多型法讀取應用 
    LineNumberReader lnr = null; 
    try 
    { 
    int i=0; 
    String line = null; 
    lnr = new LineNumberReader(new FileReader(file)); //實作行數讀取器讀取file檔案內的字串資料 
    while ( (line = lnr.readLine()) != null) 
    {//執行LOOP直到最後一行讀取為止 
    StringTokenizer st = new StringTokenizer(line,"\n\r"); //實作字串標記處理
    if (st.hasMoreTokens()) 
    { //判別是否有換行標記\n\r 
    String showannouncecycle = st.nextToken(); //讀取某行後就換下一行 
    _announcecycle.add(showannouncecycle); //將每行的字串載入_announcecycle元件處理 

    i++; 
    } 
    } 

    _log.config("announcecycle: Loaded " + i + " announcecycle."); 
    } 
    catch (IOException e1) 
    { 
    _log.log(Level.SEVERE, "Error reading announcecycle", e1); 
    } 
    finally 
    { 
    try 
    { 
    lnr.close(); //關閉行數讀取器 
    } 
    catch (Exception e2) 
    { 
    //     nothing 
    } 
    } 
    } 
    /** 
    * 顯示循環公告內容 
    * by game 
    */ 
    public void showannouncecycle(String str) 
    { 
    announceToAll(str); 
    System.out.println("公告事項: "+str ); 
    } 
    
    private void announceToAll(String msg)
{
              Collection <L1PcInstance> allpc = L1World.getInstance().getAllPlayers();
//開始------------------------------#0021              
              System.out.println("當前在綫玩家: "+ allpc.size() ); 
//結束------------------------------#0021                    
          for ( L1PcInstance pc : allpc )
                pc.sendPackets( new S_SystemMessage("\\fU[系統公告] "+msg) );
		}
}