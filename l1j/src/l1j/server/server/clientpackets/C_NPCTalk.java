/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;


import java.util.logging.Logger;

import l1j.server.server.ClientThread;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.NpcActionTable;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PolyMorph;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.npc.L1NpcHtml;
import l1j.server.server.model.npc.action.L1NpcAction;
import l1j.server.server.model.skill.L1BuffUtil;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.serverpackets.S_NPCTalkReturn;

import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Skills;
import static l1j.server.server.model.skill.L1SkillId.*;


// Referenced classes of package l1j.server.server.clientpackets:
// ClientBasePacket, C_NPCTalk

public class C_NPCTalk extends ClientBasePacket {

	private static final String C_NPC_TALK = "[C] C_NPCTalk";
	private static final int ABSOLUTE_BARRIER = 0;
	private static Logger _log = Logger.getLogger(C_NPCTalk.class.getName());

	public C_NPCTalk(byte abyte0[], ClientThread client)
			throws Exception {
		super(abyte0);
		int objid = readD();
		L1Object obj = L1World.getInstance().findObject(objid);
		L1PcInstance pc = client.getActiveChar();
		if (obj != null && pc != null) {
			L1NpcAction action = NpcActionTable.getInstance().get(pc, obj);
			if (action != null) {
				L1NpcHtml html = action.execute("", pc, obj, new byte[0]);
				if (html != null) {
					pc.sendPackets(new S_NPCTalkReturn(obj.getId(), html));
				}
				return;
			}
			if(obj instanceof L1NpcInstance){	   
				if(((L1NpcInstance)obj).getNpcId()==70599){
					    
					      L1ItemInstance adena = ItemTable.getInstance().createItem(40308);
					      adena.setCount(pc.getInventory().countItems(40308));
					      if(adena.getCount()<1000000){
					      pc.sendPackets(new S_SystemMessage("現金不足一百萬!!!!"));
					      return;
					      }else{
					      pc.getInventory().consumeItem(40308, 1000000);
					      }
					  
					    if (pc.hasSkillEffect(ABSOLUTE_BARRIER)) {
					      pc.killSkillEffectTimer(ABSOLUTE_BARRIER);
					      pc.startHpRegeneration();
					      pc.startMpRegeneration();
					      pc.startMpRegenerationByDoll();
					    }
					    removeExistSkillEffect(pc);
					    addAllBuff(pc);
				    return;
				}
				   
			}
			obj.onTalkAction(pc);
		} else {
			_log.severe("オブジェクトが見つかりません objid=" + objid);
		}
	}
	private void removeExistSkillEffect(L1PcInstance pc){
		int[] allBuffSkill ={PHYSICAL_ENCHANT_DEX, PHYSICAL_ENCHANT_STR, BLESS_WEAPON,
				ADVANCE_SPIRIT, BURNING_WEAPON,IRON_SKIN,
				ELEMENTAL_FIRE, SOUL_OF_FLAME};
		for(int i = 0; i < allBuffSkill.length; i++){
			if(pc.hasSkillEffect(i)){
				pc.removeSkillEffect(i);
				pc.killSkillEffectTimer(i);
			}
		
		}
	}
	private void addAllBuff(L1PcInstance pc){
		int[] allBuffSkill ={PHYSICAL_ENCHANT_DEX, PHYSICAL_ENCHANT_STR, BLESS_WEAPON,
								ADVANCE_SPIRIT, BURNING_WEAPON,IRON_SKIN,
								ELEMENTAL_FIRE, SOUL_OF_FLAME};
		
			L1BuffUtil.haste(pc, 3600 * 1000);
			L1BuffUtil.brave(pc, 3600 * 1000);
			for (int i = 0; i < allBuffSkill.length; i++) {
				L1Skills skill = SkillsTable.getInstance().getTemplate(
						allBuffSkill[i]);
				new L1SkillUse().handleCommands(pc, allBuffSkill[i], pc
						.getId(), pc.getX(), pc.getY(), null, skill
						.getBuffDuration() * 1000, L1SkillUse.TYPE_GMBUFF);
			}

	
	}

	@Override
	public String getType() {
		return C_NPC_TALK;
	}
}
