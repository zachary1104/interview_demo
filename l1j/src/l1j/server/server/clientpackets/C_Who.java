/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import java.util.logging.Logger;

import l1j.server.Config;
import l1j.server.server.ClientThread;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_WhoAmount;
import l1j.server.server.serverpackets.S_WhoCharinfo;
import l1j.server.server.serverpackets.S_PacketBox; //#111
import l1j.server.server.serverpackets.S_SystemMessage;//#000
// Referenced classes of package l1j.server.server.clientpackets:
// ClientBasePacket

public class C_Who extends ClientBasePacket {

	private static final String C_WHO = "[C] C_Who";
	private static Logger _log = Logger.getLogger(C_Who.class.getName());

	public C_Who(byte[] decrypt, ClientThread client) {
		super(decrypt);
		String s = readS();
		L1PcInstance find = L1World.getInstance().getPlayer(s);
		L1PcInstance pc = client.getActiveChar();

		if (find != null) {
			S_WhoCharinfo s_whocharinfo = new S_WhoCharinfo(find);
			pc.sendPackets(s_whocharinfo);
		} else {
			if (Config.ALT_WHO_COMMAND) {
				String amount = String.valueOf(L1World.getInstance()
						.getAllPlayers().size());
				S_WhoAmount s_whoamount = new S_WhoAmount(amount);
				pc.sendPackets(s_whoamount);
				
//				開始------------------- #1				
				pc.sendPackets(new S_SystemMessage(" 經驗：" + Config.RATE_XP
					           						+ "，正義：" + Config.RATE_LA
					           						+ "，友好：" + Config.RATE_KARMA
					           						+ "，金錢：" + Config.RATE_DROP_ADENA
					           						+ "，掉寶：" + Config.RATE_DROP_ITEMS
					           						+ "，衝武：" + Config.ENCHANT_CHANCE_WEAPON
					           						+ "，衝防：" + Config.ENCHANT_CHANCE_ARMOR
					           						+ "，負重：" + Config.RATE_WEIGHT_LIMIT
					           						
					           						));	
					           				
					 if (pc.isGm() == true) { //如果查看的人是GM則調出在線玩家
		
					    pc.sendPackets(new S_PacketBox(S_PacketBox.CALL_SOMETHING)); 
					}else{
	       				int i=1;
	       				String msg = "";
	       				pc.sendPackets(new S_SystemMessage("當前在線人員:"));
	       				pc.sendPackets(new S_SystemMessage("---------------------------------"));
	       	                for (L1PcInstance pc1 : L1World.getInstance().getAllPlayers()) { 	
	       	                	if(pc1.getAccessLevel() == 0){  
	       	                	
			       	               	 //pc.sendPackets(new S_SystemMessage(i+" "+pc1.getName()+"，血盟："+pc1.getClanname()+"，等級："+pc1.getLevel()+"\r\n"));	        
			       	                     
			       	               	 msg+=i+" "+pc1.getName()+"，血盟："+pc1.getClanname()+"\r\n";
			       	                	 i++;
	       	                	}	 
	       	                	 
	       	                }
	       	               pc.sendPackets(new S_SystemMessage( msg+"---------------------------------"));
	       	                					
						
					}				
				
				
			}
			// 対象が居ない場合はメッセージ表示する？わかる方修正お願いします。
		}
	}

	@Override
	public String getType() {
		return C_WHO;
	}
}
