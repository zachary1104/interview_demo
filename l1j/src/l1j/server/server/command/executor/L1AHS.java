/**
*                 This class is one of GM commands and the only purpose is allow GM to 
*                 Enable or Disable Auto Healing System in the game without restart the server.
*                 PS.
*                         future function : add one parameter "tempOff", and one the parameter is 
*          catched
*                         
*/
package l1j.server.server.command.executor;

import l1j.server.Config;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SystemMessage;
import static l1j.server.server.model.skill.L1SkillId.*;

public class L1AHS implements L1CommandExecutor {

        private L1AHS(){
                
        }public static L1CommandExecutor getInstance() {
                return new L1AHS();
        }
        @Override
        public void execute(L1PcInstance pc, String cmdName, String arg) {
                // TODO Auto-generated method stub
                if(arg.compareToIgnoreCase("on")==0){
                        Config.AHS_CONTROLLER = true;
                        pc.sendPackets(new S_SystemMessage("啟用自動喝水系統"));
                }else if(arg.compareToIgnoreCase("off")==0){
                        Config.AHS_CONTROLLER = false;
                        for(L1PcInstance p : L1World.getInstance().getAllPlayers()){
                                if(p.hasSkillEffect(250000)){
                                        p.removeSkillEffect(STATUS_AUTO_HEALING_SYSTEM);
                                }
                        }
                        pc.sendPackets(new S_SystemMessage("自動喝水系統已被遊戲管理員強制關閉"));
                }else{
                        pc.sendPackets(new S_SystemMessage("請輸入下面格式  : "+cmdName
                                        +" [on/off/tempOff]"));
                }
        }

}

