package l1j.server.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SystemMessage;

public class GameTimeController implements Runnable {
	private static Logger _log = Logger.getLogger(GameTimeController.class.getName());

	private static GameTimeController _instance;

	public static GameTimeController getInstance() {
		if (_instance == null) {
			_instance = new GameTimeController();
		}
		return _instance;
	}

	private volatile int _serverTime = 0;

	// add 重起伺服器 use weitemp&daidaigo code 2/5
	private long StartTime = System.currentTimeMillis(); // 取出目前时间(毫秒)
	// add end

	// 2003年7月3日 21:00が1月1日00:00
//開始---------------------------#001	
	private static final long BASE_TIME_IN_MILLIS_REAL = 1262075850000L;
//結束--------------------#001
	// 365日のミリ秒
	//private static final long YEAR_IN_MIILIS = 31536000000L;

	private GameTimeController() {
		//
	}

	// add 重起伺服器 use weitemp&daidaigo code 5/5
	private void broadcastToAll(String s) {
		Collection<L1PcInstance> al1pcinstance = L1World.getInstance().getAllPlayers();
		// for (int i = 0; i < al1pcinstance.length; i++)
		// al1pcinstance[i].sendPackets(new S_SystemMessage(s));
		for (L1PcInstance tg : al1pcinstance) {
			tg.sendPackets(new S_SystemMessage(s));
		}
	}

	// add end
	public Calendar getGameCalendar() {
		Calendar cal = Calendar.getInstance();
		long currentTimeMillis = System.currentTimeMillis();
		long t1 = currentTimeMillis - BASE_TIME_IN_MILLIS_REAL;
		cal.setTimeInMillis(currentTimeMillis);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		int t2 = (int) ((t1 * 6) / 1000) - 3600 * 9;
		int t3 = t2 % 3;
		cal.add(Calendar.SECOND, t2 - t3);

		// Format f = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
		// System.out.println(f.format(cal.getTime()));

		return cal;
	}

	// add end

	public int getGameTime() {
		return _serverTime;
	}

	public boolean isNowNight() {
		boolean night = false;
		int nowservertime = getGameTime();
		int nowtime = nowservertime % 86400;
		if (nowtime >= 60 * 60 * 18 || nowtime >= 0 && 60 * 60 * 6 > nowtime) { // 18～6时
			night = true;
		}
		return night;
	}

	// add 重起伺服器 use weitemp&daidaigo code 3/5
	private int myresttime() {
		// Properties properties = new Properties();
		// InputStream inputstream =
		// getClass().getResourceAsStream("/config/rates.properties");
		int myresttime = 0;
		try {
			Properties properties = new Properties();
			InputStream is = new FileInputStream(new File("./config/rates.properties"));
			properties.load(is);
			is.close();
			myresttime = Integer.parseInt(properties.getProperty("restsystime"));

		} catch (IOException e) {
			e.printStackTrace();
		}
		return myresttime;
	}

	public void run() {
		Calendar cal = Calendar.getInstance();
		while (true) {
			long currentTimeMillis = System.currentTimeMillis();
			long t1 = currentTimeMillis - BASE_TIME_IN_MILLIS_REAL;

			cal.setTimeInMillis(currentTimeMillis);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.HOUR_OF_DAY, 9);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			int t2 = (int) ((cal.getTimeInMillis() + (t1 * 6)) / 1000L);
			int t3 = t2 % 3; // 时间が3の倍数になるように调整
			_serverTime = t2 - t3;

			try {
				// add 重起伺服器 use weitemp&daidaigo code 4/5
				long ProcessTime = System.currentTimeMillis() - StartTime; // 计算处理时间
				// if((ProcessTime / 1000) / (myresttime()*60) == 1) { //3 mill
				// if((ProcessTime / 1000) / (myresttime() + 3)*60 == 1) {
				if ((ProcessTime / 1000) / (myresttime() * 60) == 1) {
					int myrestsecs = 180;
					while (myrestsecs > 0) {
						switch (myrestsecs) {
							case 180:
								broadcastToAll("伺服器將于3分鐘后重新啟動,請至安全區域準備登出。");
								System.out.println("伺服器將于3分鐘后重新啟動,請至安全區域準備登出。");
								break;
							case 120:
								broadcastToAll("伺服器將于2分鐘后重新啟動,請至安全區域準備登出。");
								System.out.println("伺服器將于2分鐘后重新啟動,請至安全區域準備登出。");
								break;
							case 60:
								broadcastToAll("伺服器將于1分鐘后重新啟動,請至安全區域準備登出。");
								System.out.println("伺服器將于1分鐘后重新啟動,請至安全區域準備登出。");
								break;
							case 30:
								broadcastToAll("伺服器將于30秒后重新啟動。");
								System.out.println("伺服器將于30秒后重新啟動。");
								break;
							case 11:
								broadcastToAll("伺服器將于10秒后重新啟動。");
								System.out.println("伺服器將于10秒后重新啟動。");
								break;
							case 10:
								broadcastToAll("伺服器將于9秒后重新啟動。");
								System.out.println("伺服器將于9秒后重新啟動。");
								break;
							case 9:
								broadcastToAll("伺服器將于8秒后重新啟動。");
								System.out.println("伺服器將于8秒后重新啟動。");
								break;
							case 8:
								broadcastToAll("伺服器將于7秒后重新啟動。");
								System.out.println("伺服器將于7秒后重新啟動。");
								break;
							case 7:
								broadcastToAll("伺服器將于6秒后重新啟動。");
								System.out.println("伺服器將于6秒后重新啟動。");
								break;
							case 6:
								broadcastToAll("伺服器將于5秒后重新啟動。");
								System.out.println("伺服器將于5秒后重新啟動。");
								break;
							case 5:
								broadcastToAll("伺服器將于4秒后重新啟動。");
								System.out.println("伺服器將于4秒后重新啟動。");
								break;
							case 4:
								broadcastToAll("伺服器將于3秒后重新啟動。");
								System.out.println("伺服器將于3秒后重新啟動。");
								break;
							case 3:
								broadcastToAll("伺服器將于2秒后重新啟動。");
								System.out.println("伺服器將于2秒后重新啟動。");
								break;
							case 2:
								broadcastToAll("伺服器將于1秒后重新啟動。");
								System.out.println("伺服器將于1秒后重新啟動。");
								break;
							case 1:
								GameServer.getInstance().shutdown(); //修正 自動重開角色資料會回溯
								break;
						}
						myrestsecs--;
						Thread.sleep(1000);
					}
				}
				// add end
				Thread.sleep(500);
			} catch (InterruptedException e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
	}

}
